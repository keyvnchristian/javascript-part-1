let name = 'Kevin';
// cannot be a reserved keyword
// should be meaningful
// cannot start with a number (1name)
// cannot contain space or hyphen (-)

// what's interesting in javascript is that the variables are dynamic
// there are two types of variables : static and dynamic
// static: have to declare the type and once declared it can't be modified, only the value
// for example in Java: String name = "Kevin";
// the compiler understand that this variable name is a String type. if later i decide to assign a number on it
// it will show us error
// while dynamic, is ...
console.log(name); //Kevin
name = 1;
console.log(name); //1
// amajing

// another extra ...
// in other languages we have so many number types like Integer, Double, Float
// javascript takes all those into a single type: number
let number = 1;
console.log(typeof number); //number
number = 1.5; // in C/Java we know this as Double or Float
console.log(typeof number); //still number