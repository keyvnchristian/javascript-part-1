let person = {
    name: 'Kevin',
    age: '25'
};

// Dot Notation
person.name = 'Christian';

// Bracket -> Used during runtime, may not know what
// attribute that is going to be accessed 
person['name'] = 'Natanael';

console.log(person);