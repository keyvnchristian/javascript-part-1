
// Performing a task
function greet(name){
    console.log('Hello ' + name);
}

// Calculating a value
function square(number){
    return number * number;
}

console.log(square(5));