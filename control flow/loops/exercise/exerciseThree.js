
// a function to determine whether the number that we pass
// is divisible by three, five, or both
// if the number is divisible by three print Hale
// if the number is divisible by five print Luya
// if the number is divisible by both three and five print Haleluya
// if the number is not divisible by any of both the numbers, print the number
// Not a number => 'Not a number'

function haleLuya(input){
    if(typeof input !== 'number')
        return NaN;
    
    if(input % 3 === 0 && input % 5 === 0)
        return 'Haleluya';

    if(input % 3 === 0)
        return 'Hale';
    
    if(input % 5 === 0)
        return 'Luya';
}