
// function that only return String properties of an object

const book = {
    name: 'Matthew',
    chapters: 28,
    author: 'Matthew',
    writtenIn: 25,
    testament: 'New'
};
showProperties(book);
function showProperties(obj){
    for(let prop in obj)
        if(typeof book[prop] === 'string')
            console.log(prop, book[prop]);
}