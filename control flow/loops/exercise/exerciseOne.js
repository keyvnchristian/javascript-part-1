
// a function that takes two numbers
// and returns the maximum of the two

function max(first, second){
    let max = first < second ? second : first;
    return max;
}

console.log(max(1,2));