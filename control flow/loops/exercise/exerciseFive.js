showNumbers(5);

//show numbers odd or even within the limit
/*
0 EVEN
1 ODD
2 EVEN
3 ODD
*/
function showNumbers(limit){
    for(let i = 0; i < limit; i++){
        console.log(i, (i % 2 == 0) ? "EVEN" : "ODD");
    }
}