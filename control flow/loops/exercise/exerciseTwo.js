
// a function to determine whether the orientation is landscape or not
function isLandscape(width, height){
    return width > height;
}

console.log(isLandscape(1080, 720));