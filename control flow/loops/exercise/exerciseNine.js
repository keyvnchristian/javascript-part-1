
const marks = [80, 80, 50];
function calculateGrade(marks){
    let sum = 0;
    for (let mark of marks)
        sum += mark;
    let average = sum / marks.length;

    if(average < 60) return 'F';
    else if(average < 70) return 'D';
    else if(average < 80) return 'C';
    else if(average < 90) return 'B';
    else return 'A';
}