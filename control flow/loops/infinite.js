
let i = 0;
while (i < 5){
    console.log(i);
    // i++; this is what happened if you forget to add this line
}

while(true){
    console.log("Do not execute");
}

// please do not execute this ... just for an example