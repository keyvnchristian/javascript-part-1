
// Loops FOR...IN

const person = {
    name: 'Kevin',
    age: 25
};

// we want to get the value from each key
// but we do want to print it thru a loop
// this is where we use bracket notation
// since we can't use person.key
for(let key in person)
    console.log(key, person[key]);

const colors = ['red', 'blue', 'green'];

for (let index in colors)
    console.log(index, colors[index]);