
// do while will execute first (execute at least once) then check the condition unlike While loop

let i = 5;
do {
    console.log("There are still " + i + " people in the queue");
    i--;
} while(i > 0);