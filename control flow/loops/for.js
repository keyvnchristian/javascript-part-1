
// loops = for, while, do while, for in, for of
// FOR

// i is short for index, common used in many loops
// print 5 times hello world
for (let i = 0; i < 5; i++)
    console.log("Hello World");

// looking for odd numbers between 1 and 5
for (let i = 1; i <= 5; i++){
    if(i % 2 !== 0) console.log(i);
}

