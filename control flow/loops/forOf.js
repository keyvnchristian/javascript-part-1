
//ES6 we have a new way on loop
// loops FOR..OF

const colors = ['red', 'green', 'blue'];

// don't have to worry about its index like for...in
for (let color of colors)
    console.log(color);