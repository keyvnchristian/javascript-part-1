
let i = 0;
// while will check the condition first and execute the statement below
// if the condition is not true then it'll never be executed like the condition of my love toward her,
// will never be executed ... :(

while(i <= 5){
    if(i % 2 !== 0) console.log(i);
    i++; // have to increment it don't forget, or else your pc will meleduk
}