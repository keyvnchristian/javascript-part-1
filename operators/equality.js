
//strict equality (same type and same value on both sides)
console.log(1 === 1); // true
console.log('1' === 1); //false comparing type string with number

// lose equality (doesn't care on type. if type doesn't match it'll automatically convert)
console.log(1 == 1); //true
console.log('1' == 1); //true -> on strict eq will return false why?
// == look at the value on the left side (string) and will automatically convert it on the right side to the left
console.log(true == 1);