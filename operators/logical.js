
// make decision on multiple conditions
// logical and, or, not

// logical AND (&&)
// only return true if both operands are TRUE
console.log(true && true);

// logical OR (||)
// only return false if both operands are FALSE
console.log(false || false);

//logical NOT (!)
//reverse boolean value
let iamGoingToBeRapturedOnlyIfImFullWithChrist = true;

console.log('Raptured', !iamGoingToBeRapturedOnlyIfImFullWithChrist); //false