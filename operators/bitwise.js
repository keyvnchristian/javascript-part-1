
// 1 = 0001
// 2 = 0010
// R = 0011 => OR => 3
// R = 0000 => AND => 0
console.log(1 | 2); // Bitwise OR -> 3
console.log(1 & 2); // Bitwise AND -> 0

// Read, Write, Execute
// 00000100 => read
// 00000010 => write
// 00000001 => execute

const readPermission = 4;
const writePermission = 2;
const executePermission = 1;

let myPermission = 0;
myPermission = myPermission | readPermission | writePermission;
console.log('My permission', myPermission);

let message = ('Permission:', myPermission & readPermission) ? 'You are permitted to read' : 'You are not permitted to read';
console.log(message);

// it's just a simple usage of bitwise so don't have to be worried much about it
// with bitwise OR we can add permission
// with bitwise AND we can check permission