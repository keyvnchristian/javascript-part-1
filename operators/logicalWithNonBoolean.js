
// Falsy
// =====
// undefined
// null
// 0
// false
// '' (empty string)
// NaN (not a number)

// anything that is not falst -> Truthy

let userColor = 'red';
let defaultColor = 'blue';
let currentColor = userColor || defaultColor;
console.log(currentColor); // red

userColor = undefined;
currentColor = userColor || defaultColor;
console.log(currentColor); // blue