# JavaScript 101

This repository contains basic Javascript, as I myself also in the process of studying Javascript itself.

## Prerequisites
- Install Node [here](https://nodejs.org/en/)
- Use any IDE that you like the most (I use VS Code)

## How to Run
```
# Let's say you want to run variables.js inside basics
cd basics

# run using Node
node variables.js
```

## Contributing
Please clone the repo and add some examples or any other basic Javascript that I might have missed. Do a pull request.